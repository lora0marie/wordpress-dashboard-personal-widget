# Wordpress : customizer.php add a menu for standard font

## Context
For my work, I worked on the design of a wordpress theme creating eco-designed website. Eco-design involves the use of only standard font.
So I thought about the implementation of a feature that allows the user to choose a font among the standard.

## Wordpress theme or child Theme
You can work on a theme like on a child theme.

#### Before
![dashboard standard](https://gitlab.com/lora0marie/wordpress-dashboard-personal-widget/raw/master/img/avant.png)

#### The result
![dashboard after modifications](https://gitlab.com/lora0marie/wordpress-dashboard-personal-widget/raw/master/img/apres.png)

# The code
To set up our new widget on the dashboard we need only 2 things.
* created our new widget
* add it to the dashboard

You can optionally do 2 other things
* remove the welcome panel |optional|
* unset the original one |optional|

These 4 things are to be added inside the functions.php file  
😊 If you are working on a un-blank file you will obviously need to add it afterwards.

### Create the content of the new one
```php
function example_dashboard_widget_function() {
  echo "<p>Lorem ipsum ...</p>";
}
```

### Add the new widget to the dashboard
[wordpress codex](https://codex.wordpress.org/Function_Reference/wp_add_dashboard_widget)
```php
function example_add_dashboard_widgets() {
  wp_add_dashboard_widget('example_dashboard_widget', 'example', 'example_dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'example_add_dashboard_widgets' );
```

### When there is more than one
```php
function example_2_dashboard_widget_function() {
  echo "<p>Lorem ipsum ...</p>";
	echo "<a href='truc'>lien site</a>";
}
```
```php
function example_add_dashboard_widgets() {
  wp_add_dashboard_widget('example_dashboard_widget', 'example', 'example_dashboard_widget_function');
  wp_add_dashboard_widget('example_2_dashboard_widget', 'example 2', 'example_2_dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'example_add_dashboard_widgets' );
```

### Remove the Welcome panel
[wordpress codex](https://codex.wordpress.org/Function_Reference/remove_action)
```php
remove_action( 'welcome_panel', 'wp_welcome_panel' );
```

### Remove all the possible widgets
[wordpress codex](https://codex.wordpress.org/Dashboard_Widgets_API)
```php
function my_custom_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']); // Activity
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // Presse-Minute
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Commentaires récents
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // Extensions
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // Liens entrant
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // Billets en brouillon
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // Blogs WordPress
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // Autres actualités WordPress
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
```

Marie LORA 🦝
