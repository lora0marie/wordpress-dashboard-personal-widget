<?php
/**
 *  content of your function.php
 */

//

/**
 * remove welcome panel *optional*
 */
 remove_action( 'welcome_panel', 'wp_welcome_panel' );

/**
 * Personal Administrater
 */
function my_custom_dashboard_widgets() {
  global $wp_meta_boxes;
  // general dashboard
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']); // Activity
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // Presse-Minute
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Commentaires récents
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // Extensions
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // Liens entrant
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // Billets en brouillon
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // Blogs WordPress
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // Autres actualités WordPress
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

function example_dashboard_widget_function() {
  echo "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>";
}
function example_2_dashboard_widget_function() {
  echo "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>";
}

function example_add_dashboard_widgets() {
  wp_add_dashboard_widget('example_dashboard_widget', 'example', 'example_dashboard_widget_function');
  wp_add_dashboard_widget('example_2_dashboard_widget', 'example 2', 'example_2_dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'example_add_dashboard_widgets' );
